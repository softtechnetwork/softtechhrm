@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/importevaluation.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		{{-- <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button> --}}
	</div>
	<div class="card-body">
		<table id="importevaluation" class="table table-xs table-hover table-bordered table-striped dataTable no-footer"
			cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>employee</th>
					<th>employee_target</th>
					<th>job</th>
					<th>kpi</th>
					<th>attitute</th>
					<th>created_at</th>
					{{-- <th></th> --}}
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="employee" class="col-sm-4 col-form-label">employee</label>
							<div class="col-sm-8">
								<input type="text" name="employee" placeholder="employee" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="employee_target" class="col-sm-4 col-form-label">employee_target</label>
							<div class="col-sm-8">
								<input type="text" name="employee_target" placeholder="employee_target"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="job" class="col-sm-4 col-form-label">job</label>
							<div class="col-sm-8">
								<select class="ls-select2" name="job">
									<option value="">== สถานะ ==</option>
									<option value="T"> เปิดใช้งาน </option>
									<option value="F"> ยกเลิก </option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="kpi" class="col-sm-4 col-form-label">kpi</label>
							<div class="col-sm-8">
								<select class="ls-select2" name="kpi">
									<option value="">== สถานะ ==</option>
									<option value="T"> เปิดใช้งาน </option>
									<option value="F"> ยกเลิก </option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="attitute" class="col-sm-4 col-form-label">attitute</label>
							<div class="col-sm-8">
								<select class="ls-select2" name="attitute">
									<option value="">== สถานะ ==</option>
									<option value="T"> เปิดใช้งาน </option>
									<option value="F"> ยกเลิก </option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="created_at" class="col-sm-4 col-form-label">created_at</label>
							<div class="col-sm-8">
								<input type="text" name="created_at" placeholder="created_at"
									class="form-control input-sm">
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop