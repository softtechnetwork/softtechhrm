@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/workday.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
		<table id="workday" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ชื่อ(ไทย)</th>
					<th>ชื่อ(อังกฤษ)</th>
					<th>ชื่อย่อ(ไทย)</th>
					<th>ชื่อ(อังกฤษ)</th>
					<th>เริ่มงาน</th>
					<th>เลือกงาน</th>
					<th>หมายเหตุ</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
						{{-- <p class="p-b-10"></p> --}}
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="name_th" class="col-sm-3 col-form-label">วัน</label>
							<div class="col-sm-9">
								<select id="day" class="ls-select2">
									<option value="">เลือกวัน</option>
									<option value="0">อาทิตย์</option>
									<option value="1">จันทร์</option>
									<option value="2">อังคาร</option>
									<option value="3">พุธ</option>
									<option value="4">พฤหัส</option>
									<option value="5">ศุกร์</option>
									<option value="6">เสาร์</option>
								</select>
							</div>
						</div>
						<div class="form-group row d-none">
							<label for="name_th" class="col-sm-3 col-form-label">ชื่อ(ไทย)</label>
							<div class="col-sm-9">
								<input type="text" name="name_th" placeholder="ชื่อ(ไทย)" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row d-none">
							<label for="name_en" class="col-sm-3 col-form-label">ชื่อ(อังกฤษ)</label>
							<div class="col-sm-9">
								<input type="text" name="name_en" placeholder="ชื่อ(อังกฤษ)" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row d-none">
							<label for="short_name_th" class="col-sm-3 col-form-label">ชื่อย่อ(ไทย)</label>
							<div class="col-sm-9">
								<input type="text" name="short_name_th" placeholder="ชื่อย่อ(ไทย)"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row d-none">
							<label for="short_name_en" class="col-sm-3 col-form-label">ชื่อย่อ(อังกฤษ)</label>
							<div class="col-sm-9">
								<input type="text" name="short_name_en" placeholder="ชื่อย่อ(อังกฤษ)"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="time_start" class="col-sm-3 col-form-label">เริ่มงาน</label>
							<div class="col-sm-9">
								<input type="time" name="time_start" placeholder="เริ่มงาน"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="time_end" class="col-sm-3 col-form-label">เลิกงาน</label>
							<div class="col-sm-9">
								<input type="time" name="time_end" placeholder="เลิกงาน" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="detail" class="col-sm-3 col-form-label">หมายเหตุ</label>
							<div class="col-sm-9">
								<textarea name="detail" class="form-control input-sm"></textarea>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop