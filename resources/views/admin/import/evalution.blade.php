@extends('admin.layouts.app')

@section('script')
{{-- <script src="{{asset('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script> --}}
<script src="{{asset('assets/admin/js/admin/import/evaluation.js')}}"></script>
{{-- <script src="{{asset('assets/admin/js/admin/custom.js')}}"></script> --}}
@stop

@section('content')
<div class="card">
	<div class="card-header ">
		<div class="card-title">
			<h3 class="pull-left">{{ isset($menu) ? $menu : '' }}</h3>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-10">
				<form id="import_evaluation" class="form-horizontal" role="form" autocomplete="off" novalidate="novalidate">
					<div class="form-group row">
						<label for="fname" class="col-md-3 control-label">Excel</label>
						<div class="col-md-9">
							<input type="file" class="form-control" name="file" required="" aria-required="true">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
						</div>
						<div class="col-md-9">
							<button class="btn btn-success" type="submit">นำเข้า</button>
						</div>
					</div>
					<hr>
					<div class="row"></div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop