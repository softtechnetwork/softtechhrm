@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/employeeexperience.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
		<table id="employeeexperience"
			class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0"
			width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ชื่อ</th>
					<th>ประเภท</th>
					<th>สถานที่</th>
					<th>ตำแหน่ง</th>
					<th>เริ่มต้น</th>
					<th>สิ้นสุด</th>
					<th>เรื่อง</th>
					<th>ระดับ</th>
					<th>สถาบัน</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
						{{-- <p class="p-b-10"></p> --}}
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">firstname</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="employee_id">
									<option value="">== firstname ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="experience_type" class="col-sm-2 col-form-label">experience_type</label>
							<div class="col-sm-10">
								<input type="text" name="experience_type" placeholder="experience_type"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="place" class="col-sm-2 col-form-label">place</label>
							<div class="col-sm-10">
								<input type="text" name="place" placeholder="place" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="level" class="col-sm-2 col-form-label">level</label>
							<div class="col-sm-10">
								<input type="text" name="level" placeholder="level" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="start_date" class="col-sm-2 col-form-label">start_date</label>
							<div class="col-sm-10">
								<input type="date" name="start_date" placeholder="start_date"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="end_date" class="col-sm-2 col-form-label">end_date</label>
							<div class="col-sm-10">
								<input type="date" name="end_date" placeholder="end_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="subject" class="col-sm-2 col-form-label">subject</label>
							<div class="col-sm-10">
								<input type="text" name="subject" placeholder="subject" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">name</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="education_level_id">
									<option value="">== name ==</option>
									@foreach ($educationlevel as $key => $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">name</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="institute_id">
									<option value="">== name ==</option>
									@foreach ($institutes as $key => $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop