@extends('admin.layouts.app')

@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>

<script src="{{asset('assets/admin/js/admin/employeeleave.js')}}"></script>
<script src="{{asset('assets/admin/js/admin/function.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
	<form id="filter">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="company_id">
					<option value="">== บริษัท ==</option>
					@foreach ($company as $item)
					<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="branch_id">
					<option value="">== สาขา ==</option>
					@foreach ($branch as $item)
					<option value="{{$item->id}}">{{$item->branch_name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="group_id">
					<option value="">== ฝ่าย ==</option>
					@foreach ($group as $item)
					<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="department_id">
					<option value="">== แผนก ==</option>
					@foreach ($department as $item)
					<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
				</select> 
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="leave_type_id">
					<option value="">== ประเภทการลา ==</option>
					@foreach ($leavetype as $item)
					<option value="{{$item->id}}">{{$item->leave_name}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="employee_id">
					<option value="">== พนักงาน ==</option>
					@foreach ($employee as $item)
					<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}</option>
					@endforeach
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<select class="ls-select2" name="status">
					<option value="">== สถานะ ==</option>
					<option value="N">รออนุมัติ</option>
					<option value="T">อนุมัติ</option>
					<option value="F">ไม่อนุมัติ</option>
				</select> 
			</div>
			<div class="col-md-3 col-sm-6">
				<button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
			</div>
		</div>
	</form>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table id="employeeleave" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>วันที่บันทึก</th>
					<th>เริ่มต้น</th>
					<th>ถึง</th>
					<th>จำนวน</th>
					<th>ชื่อ - นามสกุล</th>
					<th></th>
					<th>ประเภทการลา</th>
					<th>ช่วงเวลา</th>
					<th>หมายเหตุ</th>
					<th>อนุมัติโดย</th>
					<th></th>
					<th>อนุมัติวันที่</th>
					<th>สถานะ</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm" enctype="multipart/form-data">
	<div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-full">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">

						<input class="form-control" type="hidden" name="id">

						<div class="row">
							<div class="col-8">
								<div class="form-group row">
									<label for="duration_name" class="col-sm-3 col-form-label">ช่วงเวลา</label>
									<div class="col-sm-9">
										<select class="ls-select2" name="leave_duration_id">
											<option value="">== ช่วงเวลา ==</option>
											@foreach ($leaveduration as $key => $item)
											<option value="{{$item->id}}">{{$item->duration_name}}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="start_date" class="col-sm-3 col-form-label">เริ่มต้น</label>
									<div class="col-sm-9">
										<input type="date" name="start_date" placeholder="start_date"
											class="form-control input-sm">
									</div>
								</div>
								<div class="form-group row">
									<label for="duration_name" class="col-sm-3 col-form-label"></label>
									<div class="col-sm-9">
										<input type="time" name="start_time" class="form-control input-sm">
									</div>
								</div>
								<div class="form-group row">
									<label for="end_date" class="col-sm-3 col-form-label">ถึง</label>
									<div class="col-sm-9">
										<input type="date" name="end_date" placeholder="end_date" class="form-control input-sm">
									</div>
								</div>
								<div class="form-group row">
									<label for="duration_name" class="col-sm-3 col-form-label"></label>
									<div class="col-sm-9">
										<input type="time" name="end_time" class="form-control input-sm">
									</div>
								</div>
								<div class="form-group row">
									<label for="firstname" class="col-sm-3 col-form-label">ชื่อ - นามสกุล</label>
									<div class="col-sm-9">
										<select class="ls-select2" name="employee_id" id="employee_id">
											<option value="">== ชื่อ นามสกุล ==</option>
											@foreach ($employee as $key => $item)
											<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}
											</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="leave_name" class="col-sm-3 col-form-label">ประเภทการลา</label>
									<div class="col-sm-9">
										<select class="ls-select2" name="leave_type_id">
											<option value="">== ประเภทการลา ==</option>
											@foreach ($leavetype as $key => $item)
											<option value="{{$item->id}}">{{$item->leave_name}}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="remark" class="col-sm-3 col-form-label">หมายเหตุ</label>
									<div class="col-sm-9">
										<textarea name="remark" class="form-control input-sm"></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label for="approver_id" class="col-sm-3 col-form-label">อนุมัติโดย</label>
									<div class="col-sm-9">
										<select class="ls-select2" name="approver_id" id="approver_id">
											<option value="">== อนุมัติโดย ==</option>
											@foreach ($employee as $key => $item)
											<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}
											</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="appoved_at" class="col-sm-3 col-form-label">อนุมัติวันที่</label>
									<div class="col-sm-5">
										<input type="date" name="approved_at" placeholder="{{date('Y-m-d')}}"
											class="form-control input-sm">
									</div>
									<div class="col-sm-4">
										<input type="time" name="approved_time" placeholder="{{date('H:i:s')}}"
											class="form-control input-sm">
									</div>
								</div>
								<div class="form-group row">
									<label for="leave_result" class="col-sm-3 col-form-label">สถานะ</label>
									<div class="col-sm-9">
										<select class="ls-select2" name="leave_result">
											<option value="">== เลือกสถานะ ==</option>
											<option value="N">รออนุมัติ</option>
											<option value="T">อนุมัติ</option>
											<option value="F">ไม่อนุมัติ</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="picture_remove hidden"></div>
								<div class="form-group row" style="overflow-y: auto;">
									<label for="leave_result" class="col-sm-3 col-form-label">ไฟล์แนบ</label>
									<div class="col-9">
										<div id="leave_picture"></div>
										<hr>
										<input type="file" name="file[]" multiple="multiple">
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop