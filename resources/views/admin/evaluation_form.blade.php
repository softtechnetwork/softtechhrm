@extends('admin.layouts.app')

@section('style')

@stop

@section('script')
<script src="{{asset('assets/plugins/jquery-nestable/jquery.nestable.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.drag_handler_example').nestable();
    });
</script>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="pull-left">Evalution Form</h5>
        </div>
        <div class="card-body">

            {{-- start tab --}}
            <div class="row">
                @for($i=1;$i<=9;$i++)
                <div class="col-lg-4">
                    <div class="container-xs-height">
                        <div class="card-body card-rating">
                            <div class="social-user-profile col-xs-height text-center col-top">
                                <div class="thumbnail-wrapper d48 circular bordered b-white">
                                    <img alt="Avatar" data-src-retina="{{asset("assets/img/profiles/$i.jpg")}}"
                                        data-src="{{asset("assets/img/profiles/$i.jpg")}}"
                                        src="{{asset("assets/img/profiles/$i.jpg")}}" width="55" height="55">
                                </div>
                                <br>
                                <i class="fa fa-check-circle text-success fs-16 m-t-10"></i>
                            </div>
                            <div class="col-xs-height p-l-20">

                                <div class="col-xs-height">
                                    <p class="no-margin fs-16">David Nester</p>
                                    <p class="hint-text m-t-5 small">San Fransisco Bay | CEO at Pages.inc</p>
                                </div>

                                <div class="conts clearfix">
                                    <div class="pull-left">
                                        <div class="stars-header">J</div>
                                    </div>
                                    <div class="stars pull-right">
                                        <input class="star star-5" id="star-5_j_{{$i}}" type="radio" name="starj_{{$i}}" />
                                        <label class="star star-5" for="star-5_j_{{$i}}"></label>
                                        <input class="star star-4" id="star-4_j_{{$i}}" type="radio" name="starj_{{$i}}" />
                                        <label class="star star-4" for="star-4_j_{{$i}}"></label>
                                        <input class="star star-3" id="star-3_j_{{$i}}" type="radio" name="starj_{{$i}}" />
                                        <label class="star star-3" for="star-3_j_{{$i}}"></label>
                                        <input class="star star-2" id="star-2_j_{{$i}}" type="radio" name="starj_{{$i}}" />
                                        <label class="star star-2" for="star-2_j_{{$i}}"></label>
                                        <input class="star star-1" id="star-1_j_{{$i}}" type="radio" name="starj_{{$i}}" />
                                        <label class="star star-1" for="star-1_j_{{$i}}"></label>
                                    </div>
                                </div>

                                <div class="conts clearfix">
                                    <div class="pull-left">
                                        <div class="stars-header">K</div>
                                    </div>
                                    <div class="stars pull-right" sytle="float">
                                        <input class="star star-5" id="star-5_k_{{$i}}" type="radio" name="stark_{{$i}}" />
                                        <label class="star star-5" for="star-5_k_{{$i}}"></label>
                                        <input class="star star-4" id="star-4_k_{{$i}}" type="radio" name="stark_{{$i}}" />
                                        <label class="star star-4" for="star-4_k_{{$i}}"></label>
                                        <input class="star star-3" id="star-3_k_{{$i}}" type="radio" name="stark_{{$i}}" />
                                        <label class="star star-3" for="star-3_k_{{$i}}"></label>
                                        <input class="star star-2" id="star-2_k_{{$i}}" type="radio" name="stark_{{$i}}" />
                                        <label class="star star-2" for="star-2_k_{{$i}}"></label>
                                        <input class="star star-1" id="star-1_k_{{$i}}" type="radio" name="stark_{{$i}}" />
                                        <label class="star star-1" for="star-1_k_{{$i}}"></label>
                                    </div>
                                </div>

                                <div class="conts clearfix">
                                    <div class="pull-left">
                                        <div class="stars-header">A</div>
                                    </div>
                                    <div class="stars pull-right">
                                        <input class="star star-5" id="star-5_a_{{$i}}" type="radio" name="stara_{{$i}}" />
                                        <label class="star star-5" for="star-5_a_{{$i}}"></label>
                                        <input class="star star-4" id="star-4_a_{{$i}}" type="radio" name="stara_{{$i}}" />
                                        <label class="star star-4" for="star-4_a_{{$i}}"></label>
                                        <input class="star star-3" id="star-3_a_{{$i}}" type="radio" name="stara_{{$i}}" />
                                        <label class="star star-3" for="star-3_a_{{$i}}"></label>
                                        <input class="star star-2" id="star-2_a_{{$i}}" type="radio" name="stara_{{$i}}" />
                                        <label class="star star-2" for="star-2_a_{{$i}}"></label>
                                        <input class="star star-1" id="star-1_a_{{$i}}" type="radio" name="stara_{{$i}}" />
                                        <label class="star star-1" for="star-1_a_{{$i}}"></label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
        {{-- end tab --}}

    </div>
</div>
@stop