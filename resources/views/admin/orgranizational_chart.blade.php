@extends('admin.layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('assets/plugins/orgchart/css/jquery.orgchart.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/basicprimitives/primitives.latest.css')}}">
@stop

@section('script')
<script src="{{asset('assets/plugins/orgchart/js/jquery.orgchart.min.js')}}"></script>
<script src="{{asset('assets/plugins/basicprimitives/primitives.min.js')}}"></script>
<script src="{{asset('assets/plugins/basicprimitives/primitives.jquery.min.js')}}"></script>
<script>

    document.addEventListener('DOMContentLoaded', function () {
        $.ajax({
            type: "get",
            url: rurl + "admin/organizationalstructure/chart_json",
            dataType: "json",
            success: function (response) {
                var control;
                console.log(response);
                var options = new primitives.orgdiagram.Config();
                var items = response;
                options.items = items;
                options.pageFitMode = 1;
                options.textOrientationType = 0;
                options.childrenPlacementType = 3;
                options.navigationMode = 0;
                options.hasSelectorCheckbox = primitives.common.Enabled.False;
                $("#basicdiagram").orgDiagram(options);
            }
        });
    });
</script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{!empty($menu)?$menu:''}}</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-transparent">
                    <div id="basicdiagram" style="width: 100%; height: 640px" />
                </div>
            </div>
        </div>
    </div>
</div>
@stop