<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ isset($title) ? $title : '' }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
            font-size: 16px;
        }
        @page {
            margin: 0.4in;
        }
        @media print {
            html,
            body {
                font-size : 16px;
            }
        }
        table {
            width: 100%;
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }
        tr , td , th {
            margin: 0px;
            text-align: center;
            border: border: 1px solid #8a8a8a;
            line-height: 12px;
            padding: 0px;
        }
        img.pictureprofile {
            width:80px;
            height:auto;
            border:solid 1px #8a8a8a;
            border-radius: 5px;
        }
        .page_break { page-break-after: always; }
        .page-number:before {content: "Page " counter(page);}
    </style>
</head>
<body>
    <header>
        <center>
            <p style="font-size:20px; margin:0px;">
                {{ isset($title) ? $title : '' }}
                <br>
                <br>
            </p>
        </center>
    </header>
    <main>
        @php
            $perpage = 30;
            $loops = FLOOR( count($data)/$perpage + ( count($data) % $perpage !=0 ? 1 : 0) )
        @endphp

        @for($i=1; $i<=$loops; $i++)
            @php
                $start = (($i-1)*$perpage)+1; // (0*30)+1 = 1
                $end = $i*$perpage; // 1*30 = 30
                $end = ($end > count($data)) ? count($data) : $end ;
            @endphp
            <table>
                <tr>
                    <th>ลำดับ</th>
                    <th>รหัสพนักงาน</th>
                    <th>คำนำหน้าชื่อ</th>
                    <th>ชื่อ</th>
                    <th>นามสกุล</th>
                    <th>เพศ</th>
                    <th>ตำแหน่ง</th>
                    <th>แผนก</th>
                    <th>ฝ่าย</th>
                    <th>วันเริ่มงาน</th>
                    <th>อายุงาน(ปีเดือน)</th>
                </tr>
                @for($j=($start - 1); $j<$end; $j++)
                <tr>
                    <td>{{ ($j+1) }}</td>
                    <td>{{ $data[$j]->empcode }}</td>
                    <td>{{ $data[$j]->prename }}</td>
                    <td>{{ $data[$j]->firstname }}</td>
                    <td>{{ $data[$j]->lastname }}</td>
                    <td>{{ App\Http\Controllers\FunctionController::gender($data[$j]->gender) }}</td>
                    <td>{{ $data[$j]->lname }}</td>
                    <td>{{ $data[$j]->dname }}</td>
                    <td>{{ $data[$j]->gname }}</td>
                    <td>{{ $data[$j]->startworking_date }}</td>
                    <td>{{ App\Http\Controllers\FunctionController::monthtoyear($data[$j]->workage_month) }}</td>
                </tr>
                @endfor
            </table>
            <caption>
                {{ $i ."/". $loops }}
            </caption>
            <p style="float:right; margin:0px; padding:0px;">{{ $start ."-". $end }} จาก {{ count($data) }} รายการ</p>
            <p style="float:left; margin:0px; padding:0px;" >
                ข้อมูลวันที่ : {{ App\Http\Controllers\FunctionController::DateThai(date('Y-m-d')) }} , เวลา {{date('H:i:s')}}
            </p>
            @if($i<$loops)
            <div class="page_break"></div>
            @endif
        @endfor
    </main>
    </body>
</html>