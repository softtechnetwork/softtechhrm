<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ isset($title) ? $title : '' }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
            font-size: 16px;
        }
        @page {
            margin: 0.4in;
        }
        @media print {
            html,
            body {
                font-size : 16px;
            }
        }
        table {
            width: 100%;
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }
        tr , td , th {
            margin: 0px;
            text-align: center;
            border: border: 1px solid #8a8a8a;
            line-height: 12px;
            padding: 0px;
        }
        img.pictureprofile {
            width:80px;
            height:auto;
            border:solid 1px #8a8a8a;
            border-radius: 5px;
        }
        .page_break { page-break-after: always; }
        .page-number:before {content: "Page " counter(page);}
    </style>
</head>
<body>
    <header>
        <center>
            <p style="font-size:20px; margin:0px;">
                {{ isset($title) ? $title : '' }}
                <br>
                <br>
            </p>
        </center>
    </header>
    <main>
        @php
            $perpage = 30;
            $loops = FLOOR( count($data)/$perpage + ( count($data) % $perpage !=0 ? 1 : 0) )
        @endphp

        @for($i=1; $i<=$loops; $i++)
            @php
                $start = (($i-1)*$perpage)+1; // (0*30)+1 = 1
                $end = $i*$perpage; // 1*30 = 30
                $end = ($end > count($data)) ? count($data) : $end ;
            @endphp
            <table>
                <tr>
                    {{-- สถาบันการศึกษา , ระดับการศึกษา , วุฒิการศึกษา ,สาขาวิชา ,เริ่มต้น[การศึกษา] ,สิ้นสุด[การศึกษา] --}}
                    <th>ลำดับ</th>
                    <th>รหัสพนักงาน</th>
                    <th>คำนำหน้าชื่อ</th>
                    <th>ชื่อ</th>
                    <th>นามสกุล</th>
                    <th width="15%">สถาบันการศึกษา</th>
                    <th width="10%">ระดับการศึกษา</th>
                    <th>วุฒิการศึกษา</th>
                    <th width="10%">สาขาวิชา</th>
                    <th width="7%">เริ่มต้น</th>
                    <th width="7%">สิ้นสุด</th>
                </tr>
                @for($j=($start - 1); $j<$end; $j++)
                <tr>
                    <td>{{ ($j+1) }}</td>
                    <td>{{ $data[$j]->empcode }}</td>
                    <td>{{ $data[$j]->prename }}</td>
                    <td>{{ $data[$j]->firstname }}</td>
                    <td>{{ $data[$j]->lastname }}</td>
                    <td>{{ !empty($data[$j]->place) ? $data[$j]->place : '-' }}</td>
                    <td>{{ !empty($data[$j]->level_subject) ? $data[$j]->level_subject : '-' }}</td>
                    <td>{{ !empty($data[$j]->level_name ) ? $data[$j]->level_name  : '-' }}</td>
                    <td>{{ !empty($data[$j]->subject) ? $data[$j]->subject : '-' }}</td>
                    <td>{{ $data[$j]->start_date }}</td>
                    <td>{{ $data[$j]->end_date }}</td>
                </tr>
                @endfor
            </table>
            <caption>
                {{ $i ."/". $loops }}
            </caption>
            <p style="float:right; margin:0px; padding:0px;">{{ $start ."-". $end }} จาก {{ count($data) }} รายการ</p>
            <p style="float:left; margin:0px; padding:0px;" >
                ข้อมูลวันที่ : {{ App\Http\Controllers\FunctionController::DateThai(date('Y-m-d')) }} , เวลา {{date('H:i:s')}}
            </p>
            @if($i<$loops)
            <div class="page_break"></div>
            @endif
        @endfor
    </main>
    </body>
</html>