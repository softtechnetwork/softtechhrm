<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
            font-size: 15px;
        }
        @page {
            size: 21cm 29.7cm;
            margin: 0.4in;
        }
        @media print {
            html,
            body {
                width: 210mm;
                height: 297mm;
                font-size : 16px;
            }
        }
        table {
            width: 100%;
            border-spacing: 0;
        }
        table.datatable {
            text-align: center;
            border-spacing: 0;
        }
        .datatable td , .datatable th {
            text-align: center;
            border: border: 0.6px solid #000;
        }
        .datatable tr {
            line-height: 7.5px;
        }
        img.pictureprofile {
            width:80px;
            height:auto;
            border:solid 1px #8a8a8a;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <center>
        <p style="font-size:18px; margin:0px;">
            {{ $title }} 
            <br>
            ตั้งแต่ 
            {{ App\Http\Controllers\FunctionController::DateThai($request['date_start']) }} 
            ถึง {{ App\Http\Controllers\FunctionController::DateThai($request['date_end']) }}
        </p>
    </center>
    <hr style="margin:0px;">
    <div style="font-size:16px !important;">
        <table>
            <tr>
                <td><strong>รหัสพนักงาน</strong>&nbsp;{{ isset($employee->empcode)?$employee->empcode:'' }}</td>
                <td><strong>ชื่อ</strong>&nbsp;{{ isset($employee->firstname)?$employee->firstname:'' }}</td>
                <td><strong>นามสกุล</strong>&nbsp;{{ isset($employee->lastname)?$employee->lastname:'' }}</td>
                <td rowspan="3">
                    <img class="pictureprofile" src="{{public_path($employee->picture_profile)}}"/>
                </td>
            </tr>
            <tr>
                <td><strong>บริษัท</strong>&nbsp;{{ isset($employee->cname)?$employee->cname:'' }}</td>
                <td><strong>สังกัด</strong>&nbsp;{{ isset($employee->bname)?$employee->bname:'' }}</td>
                <td><strong>ฝ่าย</strong>&nbsp;{{ isset($employee->gname)?$employee->gname:'' }}</td>
            </tr>
            <tr>
                <td><strong>ตำแหน่ง</strong>&nbsp;{{ isset($employee->lname)?$employee->lname:'' }}</td>
                <td><strong>เวลาเข้างาน</strong>&nbsp;08:00น.</td>
                <td><strong>เวลาออกงาน</strong>&nbsp;17:00น.</td>
            </tr>
        </table>
    </div>
    <hr style="margin:0px;">
    <p  style="font-size:16px !important; margin:0px;">รายการลงเวลากะทำงาน</p>
    <table class="datatable">
        <tr>
            <th>วันที่</th>
            <th>วัน</th>
            <th>เวลาเข้า</th>
            <th>เวลาออก</th>
            <th>ชม</th>
            <th>สาย</th>
        </tr>
        @php
            $count_late = 0;
            $employee_registration = collect($employee_registration);
            $days = \Carbon\Carbon::make(\Illuminate\Support\Arr::get($request, 'month') . '-01')->daysInMonth;
        @endphp
        @for($i = 1; $i <= $days; $i++)
            @php
                $date = \Carbon\Carbon::make(\Illuminate\Support\Arr::get($request, 'month') . '-' . (sprintf("%02d", $i)))->format('Y-m-d');
                $hasData = $employee_registration->has($date);
                $item = collect();
                if ($hasData) {
                    $item = head($employee_registration->get($date));
                }
            @endphp
            <tr>
                <td>{{$date}}</td>
                <td>{{ App\Http\Controllers\FunctionController::thai_date($date,'w') }}</td>
                <td>{{head(explode('.', data_get($item, 'in_time', '-')))}}</td>
                <td>{{head(explode('.', data_get($item, 'out_time', '-')))}}</td>
                <td>{{data_get($item, 'working_time', '-')}}</td>
                <td>{{data_get($item, 'status', '-')}}</td>
            </tr>
            @php
                if(data_get($item, 'status')=='สาย'){
                    $count_late++;
                }
            @endphp
        @endfor
        @for ($i = 1; $i <= 22-$days; $i++)
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        @endfor
        <tr>
            <td style="text-right" colspan="5"><b>รวมสาย</b></td>
            <td>
                {{$count_late}}
            </td>
        </tr>
    </table>
    <p style="font-size:16px !important; margin:0px;">สรุปจำนวนวันลา</p>
    <table class="datatable">
        <tr>
            <th class="text-center">รหัส</th>
            <th class="text-center">ประเภท</th>
            <th class="text-center">ลาได้(วัน)</th>
            <th class="text-center">ลาไปแล้ว(วัน)</th>
            <th class="text-center">คงเหลือ(วัน)</th>
        </tr>
        @foreach ($leave_conclusion as $key => $item)
        <tr>
            <td class="text-center">{{$item->id}}</td>
            <td class="text-center">{{$item->leave_name}}</td>
            <td class="text-center">{{$item->amount_day}}</td>
            <td class="text-center">{{$item->used_day}}</td>
            <td class="text-center">{{$item->balance_day}}</td>
        </tr>
        @endforeach
    </table>
    <p style="float:right; margin:0px;" ><b>สถานะ : {{ $employee_confirm }}</b></p>
</body>
</html>