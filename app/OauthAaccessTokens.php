<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthAaccessTokens extends Model
{
    protected $table = 'oauth_access_tokens';
}
