<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Educationlevel extends Model
{
    protected $table = 'education_level';
    public $timestamps = true;
    public function scopeActive($query){
        return $query->where('status', 'T');
    }
}