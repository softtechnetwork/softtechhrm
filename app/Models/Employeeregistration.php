<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeregistration extends Model
{
    protected $table = 'employee_registration';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}