<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationLeavetimeType extends Model
{
    protected $table = 'notification_leavetimetype';
    public $timestamps = true;
}
