<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluationCalculateLog extends Model
{
    protected $table = 'evaluation_calculate_log';
    public $timestamps = true;
}