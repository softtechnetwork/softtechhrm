<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationLeavetimeLogs extends Model
{
    protected $table = 'notification_leavetimelogs';
    public $timestamps = true;
}
