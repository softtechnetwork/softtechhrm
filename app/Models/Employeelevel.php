<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeelevel extends Model
{
    protected $table = 'employee_level';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}