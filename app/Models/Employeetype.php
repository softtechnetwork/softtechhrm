<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeetype extends Model
{
    protected $table = 'employee_type';
    public $timestamps = true;
}