<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeevaluation extends Model
{
    protected $table = 'employee_evaluation';
    public $timestamps = true;
}