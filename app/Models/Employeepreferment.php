<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeepreferment extends Model
{
    protected $table = 'employee_preferment';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}