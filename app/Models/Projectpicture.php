<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
class Projectpicture extends Model
{
    // use SoftDeletes;
    protected $table = 'project_picture';
    public $timestamps = true;
}