<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeleaveapprovers extends Model
{
    protected $table = 'employee_leave_approvers';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}