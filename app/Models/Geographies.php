<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Geographies extends Model
{
    protected $table = 'geographies';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}