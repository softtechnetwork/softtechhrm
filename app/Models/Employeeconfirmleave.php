<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeconfirmleave extends Model
{
    protected $table = 'employee_confirm_leave';
    public $timestamps = true;
}