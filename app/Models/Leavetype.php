<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leavetype extends Model
{
    protected $table = 'leave_type';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}