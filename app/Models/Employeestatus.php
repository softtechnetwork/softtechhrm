<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeestatus extends Model
{
    protected $table = 'employee_status';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}