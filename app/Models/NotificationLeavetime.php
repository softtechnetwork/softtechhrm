<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationLeavetime extends Model
{
    protected $table = 'notification_leavetime';
    public $timestamps = true;
}
