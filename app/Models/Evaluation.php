<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $table = 'evaluation';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}