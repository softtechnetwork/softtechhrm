<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employeeleave extends Model
{
    protected $table = 'employee_leave';

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }

    public function leaveType(): BelongsTo
    {
        return $this->belongsTo(Leavetype::class);
    }

    public function getIsWorkAttribute(): bool
    {
        return $this->leaveType->is_work;
    }
}