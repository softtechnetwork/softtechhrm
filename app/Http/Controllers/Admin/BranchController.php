<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Branch;
use App\Models\Provinces;
use App\Models\Districts;
use App\Models\Subdistricts;


class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['provinces'] = Provinces::get();
		$data['districts'] = Districts::get();
		$data['subdistricts'] = Subdistricts::get();

        $data['menu'] = 'สาขา';
        return view('admin.branch')->with($data); // admin/branch
    }

    public function list(){
        $model = Branch::query();
        $model->leftjoin('provinces','branch.province_id','provinces.id');
        $model->leftjoin('districts','branch.district_id','districts.id');
        $model->leftjoin('subdistricts','branch.subdistrict_id','subdistricts.id');
        $model->select([
            'provinces.id as provincesid'
            ,'districts.id as districtsid'
            ,'subdistricts.id as subdistrictsid'
            ,'branch.*'
            ,'branch.id as branchid'
            ,'districts.name_th as dnameth'
            ,'provinces.name_th as pnameth'
            ,'subdistricts.name_th as snameth'
            ,'branch.status as branchstatus'
        ]);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->branchid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->branchid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->editColumn('branchstatus',function($rec){
                    if($rec->status=='T'){
                        return '<span class="badge badge-success">ใช้งาน</span>';
                    }else{
                        return '<span class="badge badge-danger">ไม่ใช้งาน</span>';
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action','branchstatus'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            unset($request['id']);
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Branch::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Branch::find($id)){
                $result['working_time_start'] = !empty($result['working_time_start']) ? (explode('.',$result['working_time_start'])[0]) : '';
                $result['working_time_end'] = !empty($result['working_time_end']) ? (explode('.',$result['working_time_end'])[0]) : '';
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Branch::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Branch::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}