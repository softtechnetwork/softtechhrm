<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Subdistricts;
use App\Models\Districts;
use App\Models\Provinces;


class SubdistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['districts'] = Districts::get();
        $data['provinces'] = Provinces::get();
        $data['menu'] = 'ตำบล/แขวง';
        return view('admin.subdistricts')->with($data);
    }

    public function list(){
        $model = Subdistricts::query();
        $model->leftjoin('districts','subdistricts.district_id','districts.id');
        $model->leftjoin('provinces','districts.province_id','provinces.id');
        $model->select(['districts.*','districts.id as districtsid','subdistricts.*','subdistricts.id as subdistrictsid','districts.name_th as dnameth','provinces.name_th as pnameth']);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->subdistrictsid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->subdistrictsid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Subdistricts::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Subdistricts::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Subdistricts::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Subdistricts::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}