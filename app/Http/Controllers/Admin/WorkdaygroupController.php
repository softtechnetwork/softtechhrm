<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Workdaygroup;
use App\Models\Workday;


class WorkdaygroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['workday'] = Workday::get();

        $data['menu'] = 'กลุ่มวันทำงาน';
        return view('admin.workdaygroup')->with($data);
    }

    public function list(){
        $model = Workdaygroup::query();
        $model->select(['workday_group.*','workday_group.id as workday_groupid']);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->workday_groupid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->workday_groupid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                // ->editColumn('group_workday',function($rec){
                //     $value = \App\Models\Workday::whereIn('id',json_decode($rec->group_workday))->get();
                //     $s = [];
                //     // return json_encode($value);
                //     foreach($value as $key => $item){
                //         // $s .= $item->day_name . (!empty($item->detail)) ? $item->detail :'' ."</br>";
                //         $s[] = $item->day_name;
                //     }
                //     return json_encode($s);
                // })
                ->addIndexColumn()
                ->rawColumns(['action','group_workday'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->group_workday){
            $request['group_workday'] = json_encode($request->group_workday);
        }else{
            $request['group_workday'] = [];
        }
        
        if(empty($request->id)){
            unset($request['id']);
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Workdaygroup::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Workdaygroup::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->group_workday){
            $request['group_workday'] = ($request->group_workday);
        }else{
            $request['group_workday'] = null;
        }
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Workdaygroup::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Workdaygroup::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}