<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Employeeevaluation;
use App\Models\Importevaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ImportevaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['menu'] = 'นำออกข้อมูลการประเมิน';
        return view('admin.importevaluation')->with($data); // admin/importevaluation
    }

    public function list(){
        $model = Importevaluation::query();
        $model->select(['import_evaluation.*']);
        // $model->select(['import_evaluation.*','import_evaluation.id as import_evaluationid']);
        return  \DataTables::eloquent($model)
                // ->addColumn('action',function($rec){
                //     $str = '
                //         <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->import_evaluationid.'">
                //             <i class="fa fa-edit"></i>
                //         </a>
                //         <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->import_evaluationid.'">
                //             <i class="fa fa-trash"></i>
                //         </a>
                //     ';
                //     return $str;
                // })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    public function sync()
    {
        Importevaluation::query()->truncate();

//        $employees = Employeeevaluation::query()
//            ->select(['employee_id', 'employee_target_id'])
//            ->get();
//
//        $employeeIds = clone $employees;
//        $employeeIds = $employeeIds->groupBy('employee_id')->toArray();
//        $employeeTargetIds = $employees->groupBy('employee_target_id')->toArray();
//        $employeeTargetIds = array_filter(array_keys($employeeTargetIds));
//        $employeeIds = array_filter(array_keys($employeeIds));
//        $employees = Employee::query()->select('id', 'empcode')->whereIn('id', $employeeIds)->get();
//        $employeeTargets = Employee::query()->select(['id', 'empcode'])->whereIn('id', $employeeTargetIds)->get();

//        Employeeevaluation::query()->chunk(1000, function($query) use ($employeeTargets, $employees) {
//            foreach ($query as $item) {
//                $evaluationAbility = json_decode($item->evaluation_ability, JSON_UNESCAPED_UNICODE);
//
//                $job = in_array('1', $evaluationAbility) ? 'T' : 'F';
//                $kpi = in_array('2', $evaluationAbility) ? 'T' : 'F';
//                $attitude = in_array('3', $evaluationAbility) ? 'T' : 'F';
//
//                Importevaluation::query()->insert(
//                    [
//                        'employee' => $employees->find($item->employee_id)->empcode,
//                        'employee_target' => $employeeTargets->find($item->employee_target_id)->empcode,
//                        'job' => $job,
//                        'kpi' => $kpi,
//                        'attitute' => $attitude,
//                    ]
//                );
//            }
//        });

        $query = DB::select(
            <<<SQL
SELECT
    e1.empcode
    ,e2.empcode as empcode_target
    ,(CASE WHEN (ee.evaluation_ability  LIKE '%"1"%') THEN 'T'
        ELSE ''
    END) as job
    ,(CASE WHEN (ee.evaluation_ability  LIKE '%"2"%') THEN 'T'
        ELSE ''
    END) as kpi
    ,(CASE WHEN (ee.evaluation_ability  LIKE '%"3"%') THEN 'T'
        ELSE ''
    END) as attitute
    ,ee.created_at
FROM employee_evaluation ee
JOIN employee e1 ON e1.id = ee.employee_id
JOIN employee e2 ON e2.id = ee.employee_target_id;
SQL
        );

        foreach ($query as $item) {
            Importevaluation::query()->insert(
                [
                    'employee' => $item->empcode,
                    'employee_target' => $item->empcode_target,
                    'job' => $item->job,
                    'kpi' => $item->kpi,
                    'attitute' => $item->attitute,
                    'created_at' => $item->created_at,
                ]
            );
        }

        return "คุณซิงค์ข้อมูลสำเร็จ!";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Importevaluation::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Importevaluation::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if($result = Importevaluation::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Importevaluation::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}