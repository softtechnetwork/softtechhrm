<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Employeeconfirm;
use App\Models\Employee;
use App\Models\Groups;
use App\Models\Department;


class EmployeeconfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employee'] = Employee::get();
        $data['groups'] = Groups::get();
        $data['departments'] = Department::get();

        $data['menu'] = 'รายงานยืนยันการลงเวลา';
        return view('admin.employeeconfirm')->with($data); // admin/employeeconfirm
    }

    public function list(Request $request){
        $month = isset($request->month)? (string)$request->month : (string)date('Y-m');
        $model = Employee::query();
        $model->leftjoin('employee_confirm',function ($join) use ($month) {
            $join->on('employee_confirm.employee_id' , 'employee.id');
            $join->where(\DB::raw('employee_confirm.month'),'LIKE','%'.$month.'%');
        });
        $model->select([
            'employee.id as eid'
            ,'employee.firstname'
            ,'employee.lastname'
            ,\DB::RAW("employee.firstname + ' ' + employee.lastname as employee_name")
            ,'employee.empcode'
            ,'employee_confirm.id as employee_confirmid'
            ,'employee_confirm.*'
        ]);
        if(isset($request->status)){
            $model->where(\DB::raw('employee_confirm.status'),$request->status);
        }
        if(isset($request->group_id)){
            $model->where('employee.group_id',$request->group_id);
        }
        if(isset($request->department_id)){
            $model->where('employee.department_id',$request->department_id);
        }
        if(isset($request->employee_id)){
            $model->where('employee.id',$request->employee_id);
        }

        return  \DataTables::eloquent($model)
                ->editColumn('status',function($rec){
                    $str = "";
                    switch ($rec->status) {
                        case 'T':
                           $str = '<h5><span class="label label-success">ยืนยัน</span></h5>';
                            break;
                        
                        case 'F':
                            $str = '<h5><span class="label label-danger">ไม่ยืนยัน</span></h5>';
                            break;
                        
                        default:
                            $str = '<h5><span class="label label-info">รอยืนยัน</span></h5>';
                            break;
                    }
                    return $str;
                })
                ->addColumn('action',function($rec){
                    $str = '
                        <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->employee_confirmid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employee_confirmid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action','status'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Employeeconfirm::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employeeconfirm::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if($result = Employeeconfirm::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employeeconfirm::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}