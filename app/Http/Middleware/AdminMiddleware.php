<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Check that yes, the system administrator
        if( Auth::guard('admin')->check() ) {
            $getmenuaccess = json_decode( \App\AdminUser::find(\Auth::guard('admin')->user()->id)->access_menu );
            $getmenuurl = \App\AdminMenu::whereIn("id",$getmenuaccess)->get();
            foreach($getmenuurl as $key => $value){
                if(substr($value->menu_url,5)!=''){
                    //check menu access
                    if(strpos(\Request::path()."/",substr($value->menu_url,5)."/")!=false){
                        $check[] = strpos(\Request::path(),substr($value->menu_url,5));
                    }
                }
            }
            if(!empty($check) || \Request::path()=='admin'){
                return $next($request);
            }else{
                abort(403, 'การกระทำที่ไม่ได้รับอนุญาต กรุณาติดต่อผุ้ดูแลระบบ');
            }
        } else {
            return redirect('admin/login');
        }
    }
}
