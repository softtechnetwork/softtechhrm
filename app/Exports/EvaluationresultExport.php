<?php
namespace App\Exports;

use App\Models\Evaluationresult;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EvaluationresultExport implements FromView
{
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('admin.report.export.export_evaluationresult',$this->data);
    }
}