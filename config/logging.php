<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            //'channels' => ['daily'],
            'channels' => [
                'api','api_error',
                'register','login','change_password','personal_details','registration','registration_destroy','news_list','news_detail','corporateinformation',
                'leave_employee','leave_approver','leave_report','leave','leave_detail','leave_type','leave_duration','confirm',
                'conclusion_monthly','jobdescription','employee_level','structure_top','structure_main','get_under','post_evaluation',
                'get_evaluation','evaluation_grade_calculate','get_score_history', 'check_post_evaluation', 'post_image', 'report_registration_leave',
                'get_employee', 'project_create', 'project', 'project_delete', 'result_desc', 'project_task_create', 'project_task_update',
                'todolist_get', 'todolist_show', 'todolist_create', 'todolist_update', 'todolist_delete',
                'sendpushCustom', 'sendpushAll', 'addDevice','get_log_notification', 'update_log_notification',
                'get_notification_forgetleavetimetype', 'post_notification_forgetleavetime', 'change_profilepicture',
            ],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
        'custom_error' => [
            //'driver' => 'single',
            'driver' => 'daily',
            'level'  => 'error',
            'path'   => storage_path('logs/usage_log.log'),
            'bubble' => false
        ],
        'custom_info' => [
            //'driver' => 'single',
            'driver' => 'daily',
            'level'  => 'info',
            'path'   => storage_path('logs/usage_log.log'),
            'bubble' => false
        ],

        //API Logs
        'api' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/api/api.log'), 'bubble' => false],
        'api_error' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/api/api.log'), 'bubble' => false],

        'register' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/register/register.log'), 'bubble' => false],
        'login' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/login/login.log'), 'bubble' => false],
        'logout' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/logout/logout.log'), 'bubble' => false],
        'change_password' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/change_password/change_password.log'), 'bubble' => false],
        'personal_details' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/personal_details/personal_details.log'), 'bubble' => false],
        'registration' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/registration/registration.log'), 'bubble' => false],
        'registration_destroy' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/registration_destroy/registration_destroy.log'), 'bubble' => false],
        'news_list' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/news_list/news_list.log'), 'bubble' => false],
        'news_detail' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/news_detail/news_detail.log'), 'bubble' => false],
        'corporateinformation' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/corporateinformation/corporateinformation.log'), 'bubble' => false],
        
        'leave_employee' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/leave_employee/leave_employee.log'), 'bubble' => false],
        'leave_approver' => ['driver' => 'daily', 'level'  => 'info','path'   => storage_path('logs/leave_approver/leave_approver.log'), 'bubble' => false],
        'leave_report' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/leave_report/leave_report.log'), 'bubble' => false ],
        'leave' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/leave/leave.log'), 'bubble' => false ],
        'leave_detail' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/leave_detail/leave_detail.log'), 'bubble' => false ],
        'leave_type' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/leave_type/leave_type.log'), 'bubble' => false ],
        'leave_duration' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/leave_duration/leave_duration.log'), 'bubble' => false ],
        'confirm' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/confirm/confirm.log'), 'bubble' => false ],
        
        'conclusion_monthly' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/conclusion_monthly/conclusion_monthly.log'), 'bubble' => false ],
        'jobdescription' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/jobdescription/jobdescription.log'), 'bubble' => false ],
        'employee_level' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/employee_level/employee_level.log'), 'bubble' => false ],
        'structure_top' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/structure_top/structure_top.log'), 'bubble' => false ],
        'structure_main' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/structure_main/structure_main.log'), 'bubble' => false ],
        'get_under' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_under/get_under.log'), 'bubble' => false ],
        'post_evaluation' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/post_evaluation/post_evaluation.log'), 'bubble' => false ],
        'get_evaluation' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_evaluation/get_evaluation.log'), 'bubble' => false ],
        'evaluation_grade_calculate' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/evaluation_grade_calculate/evaluation_grade_calculate.log'), 'bubble' => false ],
        
        
        'get_score_history' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_score_history/get_score_history.log'), 'bubble' => false ],
        'check_post_evaluation' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/check_post_evaluation/check_post_evaluation.log'), 'bubble' => false ],
        'post_image' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/post_image/post_image.log'), 'bubble' => false ],
        'report_registration_leave' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/report_registration_leave/report_registration_leave.log'), 'bubble' => false ],
        
        'get_employee' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_employee/get_employee.log'), 'bubble' => false ],
        'project_create' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project_create/project_create.log'), 'bubble' => false ],
        'project' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project/project.log'), 'bubble' => false ],
        'project_delete' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project_delete/project_delete.log'), 'bubble' => false ],
        'project_list' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project_list/project_list.log'), 'bubble' => false ],
        'project_task_create' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project_task_create/project_task_create.log'), 'bubble' => false ],
        'project_task_update' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/project_task_update/project_task_update.log'), 'bubble' => false ],
        
        'todolist_get' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/todolist_get/todolist_get.log'), 'bubble' => false ],
        'todolist_show' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/todolist_show/todolist_show.log'), 'bubble' => false ],
        'todolist_create' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/todolist_create/todolist_create.log'), 'bubble' => false ],
        'todolist_update' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/todolist_update/todolist_update.log'), 'bubble' => false ],
        'todolist_delete' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/todolist_delete/todolist_delete.log'), 'bubble' => false ],
        
        'sendpushCustom' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/sendpushCustom/sendpushCustom.log'), 'bubble' => false ],
        'sendpushAll' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/sendpushAll/sendpushAll.log'), 'bubble' => false ],
        'addDevice' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/addDevice/addDevice.log'), 'bubble' => false ],
        'get_log_notification' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_log_notification/get_log_notification.log'), 'bubble' => false ],
        'update_log_notification' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/update_log_notification/update_log_notification.log'), 'bubble' => false ],
        'count_noti_unread' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/count_noti_unread/count_noti_unread.log'), 'bubble' => false ],

        'get_notification_forgetleavetimetype' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/get_notification_forgetleavetimetype/get_notification_forgetleavetimetype.log'), 'bubble' => false ],
        'post_notification_forgetleavetime' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/post_notification_forgetleavetime/post_notification_forgetleavetime.log'), 'bubble' => false ],
        'change_profilepicture' => ['driver' => 'daily', 'level'  => 'info', 'path'   => storage_path('logs/change_profilepicture/change_profilepicture.log'), 'bubble' => false ],

    ],

];
