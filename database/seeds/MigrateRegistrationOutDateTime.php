<?php

use App\Models\Employeeregistration;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MigrateRegistrationOutDateTime extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employeeRegistrations = Employeeregistration::query()
            ->whereNotNull('out_date')
            ->whereNotNull('out_time')
            ->whereRaw("CONVERT(DATE, CONVERT(VARCHAR, in_date) + ' ' + CONVERT(VARCHAR, in_time)) > CONVERT(DATE, CONVERT(VARCHAR, out_date) + ' ' + CONVERT(VARCHAR, out_time))")
            ->get();

        $employeeRegistrations->each(function ($employeeRegistration) {
            $employeeRegistration->update(['out_date' => Carbon::make($employeeRegistration->out_date)->addDay()->format('Y-m-d')]);
        });
    }
}
