var Example = (function() {
  var handleTables = function() {
    var table = $("#" + menu).DataTable({
      responsive: true,
      serverSide: true,
      processing: true,
      ajax: {
        url: rurl + "install/list",
        type: "GET",
      },
      columns: [
        {
          data: "DT_RowIndex",
          name: "DT_RowIndex",
          orderable: false,
          searchable: false,
          className: "text-center",
        },
        {
          data: "Tables_in_" + database,
          name: "Tables_in_" + database,
        },
      ],
    });

    $("#" + menu + " tbody").on("click", "tr", function() {
      if ($(this).hasClass("selected")) {
        $(this).removeClass("selected");
      } else {
        table.$("tr.selected").removeClass("selected");
        $(this).addClass("selected");
        var str = $(this).find("td")[1];
        getcolumns(str.textContent);
      }
    });

    function getcolumns(tablename) {
      $("#tablename").html(tablename);
      $("[name='table']").val(tablename);
      $(".column").removeClass("d-none");
      $("#column")
        .DataTable()
        .clear()
        .destroy();
      $("#column").DataTable({
        responsive: true,
        processing: true,
        paging: false,
        ajax: {
          url: rurl + "install/column",
          type: "POST",
          data: {
            table: tablename,
          },
        },
        columns: [
          {
            data: "checkall",
            name: "checkall",
            orderable: false,
            searchable: false,
            className: "text-center",
          },
          {
            data: "Field",
            name: "Field",
          },
          {
            data: "Type",
            name: "Type",
          },
          {
            data: "validate",
            name: "validate",
            orderable: false,
            searchable: false,
          },
          {
            data: "inputtype",
            name: "inputtype",
            orderable: false,
            searchable: false,
          },
          {
            data: "null",
            name: "null",
            orderable: false,
            searchable: false,
          },
          {
            data: "null",
            name: "null",
            orderable: false,
            searchable: false,
          },
        ],
        drawCallback: function(settings) {
          handlePagelink("column");
          // $(".inputtype").select2()
        },
      });

      $("html, body").animate(
        {
          scrollTop: $(".column").offset().top,
        },
        1500
      );
    }

    $(document).on("click", ".btn-removemvc", function() {
      $this = $(this);
      swal(
        {
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์ MVC นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false,
        },
        function() {
          // console.log($('tr.selected td:last').text());
          deleteMVC($("tr.selected td:last").text());
        }
      );
    });
  };

  var deleteMVC = function(value) {
    var id = value;

    $.ajax({
      url: rurl + "install/removemvc",
      type: "DELETE",
      data: {
        tablename: id,
      },
      success: function(data) {
        console.log(data);
        $("#example")
          .DataTable()
          .ajax.reload(null, false);
        swal("ยินดีด้วย!", data, "success");
      },
      error: function(data) {
        swal("พบข้อผิดผลาด!", data, "error");
      },
    });
  };

  var handleCheckall = function(id) {
    $(document).on("click", "#" + id + ' [name="checkall"]', function() {
      var btn_checkall = $(this).is(":checked");
      var findcheckall = $('[name*="[use]"]');
      if (btn_checkall == true) {
        findcheckall.prop("checked", true);
      } else {
        findcheckall.prop("checked", false);
      }
    });
  };

  var handleChecksingle = function() {
    $(document).on("click", $("#column [name*='[use]']"), function() {
      var check = $("[name*='[use]']");
      var checked = $("tbody [name*='[use]']:checked");
      var notcheck = $("tbody [name*='[use]']:not(:checked)");
      var checkall = $("[name='checkall']");

      checked
        .closest("tr")
        .find("select.inputtype")
        .prop("required", true);
      notcheck
        .closest("tr")
        .find("select.inputtype")
        .prop("required", false);

      if (check.length == checked.length && check.length != 0) {
        checkall.prop("checked", true);
      } else {
        checkall.prop("checked", false);
      }
    });
  };

  var handlePagelink = function(id) {
    var button_checkall = $("#" + id + ' [name="checkall"]');
    var findcheckall = $("#" + id).find('[type="checkbox"]');
    if (button_checkall.is(":checked") == true) {
      findcheckall.prop("checked", true);
    } else {
      findcheckall.prop("checked", false);
    }
  };

  var handleChangeinputttype = function() {
    $(document).on("change", ".inputtype", function() {
      var $this = $(this);
      var $name = $this
        .closest("tr")
        .find("td")
        .get(1).textContent;

      if (
        $(this).val() == "dropdown" ||
        $(this).val() == "checkbox" ||
        $(this).val() == "radio"
      ) {
        var td = $this.closest("td");
        td.next()
          .next()
          .html("");
        $.ajax({
          type: "get",
          url: rurl + "install/detailvalue",
          dataType: "json",
          success: function(response) {
            var str =
              "<select class='source' name=\"" + $name + '[source]" required>';
            str += '<option value=""> == select table == </option>';
            $.each(response, function(key, value) {
              str +=
                "<option value=" +
                value["Tables_in_" + database] +
                ">" +
                (key + 1) +
                "." +
                value["Tables_in_" + database] +
                "</option>";
            });
            str += "</select>";
            $this
              .closest("td")
              .next()
              .html(str);
          },
        });
      } else {
        var td = $this.closest("td");
        td.next().html(null);
        td.next()
          .next()
          .html(
            "<input type='text' name=\"" +
              $name +
              '[name]" value="' +
              $name +
              '" readonly>'
          );
      }
    });

    $(document).on("click", ".source", function() {
      var $this = $(this);
      var $name = $this
        .closest("tr")
        .find("td")
        .get(1).textContent;
      $.ajax({
        type: "get",
        url: rurl + "install/detailvalue",
        data: { table: $(this).val() },
        dataType: "json",
        success: function(response) {
          var str = '<select name="' + $name + '[id]" required>';
          str += '<option value=""> == select value == </option>';
          $.each(response, function(key, value) {
            str +=
              "<option value=" + value.Field + ">" + value.Field + "</option>";
          });
          str += "</select>";
          str += '<select name="' + $name + '[name]" required>';
          str += "<option value=''> == select name == </option>";
          $.each(response, function(key, value) {
            str +=
              "<option value=" + value.Field + ">" + value.Field + "</option>";
          });
          str += "</select>";
          $this
            .closest("td")
            .next()
            .html(str);
        },
      });
    });
  };

  var handleForm = function() {
    $(".validateForm").submit(function(e) {
      e.preventDefault();
      var form = $(this);
      // console.log(form)
      $.ajax({
        type: "POST",
        url: rurl + "install/createmvc",
        data: form.serialize(),
        dataType: "json",
        success: function(data) {
          swal("ยินดีด้วย!", data["result"], "success");
        },
        error: function(data) {
          swal("พบข้อผิดผลาด!", data["result"], "error");
        },
      });
    });
  };

  return {
    // main function to initiate the module
    init: function() {
      handleTables();
      handleCheckall("column");
      handleChangeinputttype();
      handleForm();
      handleChecksingle();
    },
  };
})();

jQuery(document).ready(function() {
  Example.init();
});
