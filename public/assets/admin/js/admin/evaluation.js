var Evaluation = (function() {
  var handleTables = function() {
    var table = $("#evaluation").DataTable({
      responsive: true,
      serverSide: true,
      processing: true,
      ajax: rurl + "admin/evaluation/list",
      language: { url: rurl + "assets/plugins/datatable_th.json" },
      columns: [
        {
          data: "DT_RowIndex",
          name: "DT_RowIndex",
          orderable: false,
          searchable: false,
          className: "text-center",
        },
        { data: "evaluation_name", name: "evaluation.evaluation_name" },
        { data: "detail", name: "evaluation.detail" },
        { data: "evaluation_start", name: "evaluation.evaluation_start" },
        { data: "evaluation_end", name: "evaluation.evaluation_end" },
        {
          data: "status",
          name: "evaluation.status",
          orderable: false,
          searchable: false,
        },
        {
          data: "action",
          orderable: false,
          searchable: false,
        },
      ],
    });
  };

  var handleValidation = function() {
    var form = $(".validateForm");
    var btn = $('.validateForm [type="submit"]');

    form.validate({
      errorElement: "span", // default input error message container
      errorClass: "help-block help-block-error", // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "", // validate all fields including form hidden input

      rules: {
        evaluation_name: { required: true },
        evaluation_start: { required: true },
        evaluation_end: { required: true },
      },

      highlight: function(element) {
        // hightlight error inputs
        $(element)
          .closest(".form-group .form-control")
          .addClass("is-invalid"); // set invalid class to the control group
      },
      unhighlight: function(element) {
        // revert the change done by hightlight
        $(element)
          .closest(".form-group .form-control")
          .removeClass("is-invalid") // set invalid class to the control group
          .closest(".form-group .form-control")
          .addClass("is-valid");
      },
      errorPlacement: function(error, element) {
        if (
          element.parent(".input-group").length ||
          element.prop("type") === "checkbox" ||
          element.prop("type") === "radio"
        ) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      },
      success: function(label) {
        label.closest(".form-group .form-control").removeClass("is-invalid"); // set success class to the control group
      },
      submitHandler: function(element) {
        btn.prop("disabled", true);
        $.ajax({
          type: "post",
          url: rurl + "admin/evaluation",
          data: $(element).serialize(),
          dataType: "html",
          success: function(data) {
            btn.prop("disabled", false);
            $('[data-dismiss="modal"]').trigger("click");
            $(".validateForm").removeClass("formadd");
            $(".validateForm").removeClass("formedit");
            $("#evaluation")
              .DataTable()
              .ajax.reload(null, false);
            swal("ยินดีด้วย!", data, "success");
          },
          error: function(data) {
            $(".validateForm").removeClass("formadd");
            $(".validateForm").removeClass("formedit");
          },
        });
      },
    });
  };

  var handleButton = function() {
    $(document).on("click", ".btn-delete", function() {
      $this = $(this);
      swal(
        {
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false,
        },
        function() {
          deleteUser($this);
        }
      );
    });

    $(document).on("click", ".btn-add", function() {
      $('[name="id"]').val(null);

      //remove form class
      $(".validateForm").removeClass("formadd");
      $(".validateForm").removeClass("formedit");

      $(".validateForm").trigger("reset");
      $(".validateForm").addClass("formadd");
      $(".validateForm").removeAttr("data-id");

      //add value to form example
      $(".ls-select2").select2();
    });

    $(document).on("click", ".btn-edit", function(btn) {
      //remove form class
      $(".validateForm").removeClass("formadd");
      $(".validateForm").removeClass("formedit");

      var id = $(this).data("id");
      $('[name="id"]').val(id);

      var selector = $(".validateForm");
      selector.addClass("formedit");
      selector.find('[type="submit"]').removeAttr("data-id");
      selector.find('[type="submit"]').attr("data-id", id);
      $.ajax({
        type: "get",
        url: rurl + "admin/evaluation/" + id,
        dataType: "json",
        success: function(data) {
          Object.entries(data).forEach((entry) => {
            $('[name="' + entry[0] + '"]').val(entry[1]);
          });
          $(".ls-select2").select2();
          $(".validateForm .modal").modal("show");
        },
        error: function(data) {
          swal("ไม่พบข้อมูลที่ท่านต้องการ!", data.responseTextta, "error");
        },
      });
    });

    $(document).on("click", ".btn-structure", function(btn) {
      // $("#chart_container").html('');
      $this = $(this).data("id");
      $.ajax({
        type: "get",
        url: rurl + "admin/organizationalstructure/chart_json/" + $this,
        dataType: "json",
        success: function(response) {
          var items = response;
          var options = new primitives.orgdiagram.Config();
          options.items = items;
          options.pageFitMode = 1;
          options.textOrientationType = 0;
          options.childrenPlacementType = 3;
          options.navigationMode = 0;
          options.hasSelectorCheckbox = primitives.common.Enabled.False;
          // $("#chart_container").orgDiagram(options);
          // $("#chart_container").orgDiagram(options, items);
          $("#chart_container").orgDiagram(
            options,primitives.orgdiagram.UpdateMode.Recreate
          );
          $("#chart_container").orgDiagram("update",primitives.orgdiagram.UpdateMode.Refresh);
 
          
          $("#modalChart").modal("show");
        },
      });
    });

    $(document).on("click", ".btn-save-structure", function(btn) {
      console.log($(this).data("id"));
      $this = $(this).data("id");
      swal(
        {
          title: "คุณแน่ใจไหม?",
          text: "เมื่อคุณกดยืนยัน จะไม่สามารถกู้คืนแผนผังองค์กรก่อนหน้าได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false,
        },
        function() {
          $.ajax({
            type: "post",
            url:
              rurl +
              "admin/organizationalstructure/structure_evaluation/" +
              $this,
            dataType: "json",
            success: function(response) {
              console.log(response);
              swal("การบันทึก!", "สำเร็จ", "success");
            },
            error: function(data) {
              swal("พบข้อผิดผลาด!", data.result_desc, "error");
            },
          });
        }
      );
    });

    $(document).on("click", '.formadd [type="submit"]', function() {
      handleValidation();
    });

    $(document).on("click", '.formedit [type="submit"]', function(e) {
      handleValidation();
    });
  };

  var deleteUser = function(value) {
    var id = value.data("id");
    var token = value.data("token");

    $.ajax({
      url: rurl + "admin/evaluation/" + id,
      type: "DELETE",
      data: {
        _method: "delete",
        _token: token,
        _id: id,
      },
      success: function(data) {
        $("#evaluation")
          .DataTable()
          .ajax.reload(null, false);
        swal("ยินดีด้วย!", data, "success");
      },
      error: function(data) {
        swal("พบข้อผิดผลาด!", data, "error");
      },
    });
  };

  return {
    // main function to initiate the module
    init: function() {
      handleTables();
      handleButton();
    },
  };
})();

jQuery(document).ready(function() {
  Evaluation.init();
});
