<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\APIController@register');
Route::get('login', 'API\APIController@formlogin');
Route::post('login', 'API\APIController@login');
Route::post('logout', 'API\APIController@logout');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('change_password', 'API\APIController@change_password');
    Route::post('details', 'API\APIController@details');
    Route::get('personal_details', 'API\APIController@personal_details');
    Route::get('personal_registration', 'API\APIController@registration_list');
    Route::post('registration', 'API\APIController@registration');
    Route::post('registration_destroy', 'API\APIController@registration_destroy');
    Route::get('news_list', 'API\APIController@news_list');
    Route::get('news_detail/{id}', 'API\APIController@news_detail');
    Route::get('corporateinformation/{id}', 'API\APIController@corporateinformation');
    Route::get('leave_employee', 'API\APIController@leave_employee');
    Route::get('leave_approver/{type}', 'API\APIController@leave_approver');
    Route::get('leave_report', 'API\APIController@leave_report');
    Route::post('leave', 'API\APIController@leave');
    Route::post('leave_test', 'API\APIController@leave_test');
    Route::get('leave_detail/{id}', 'API\APIController@leave_detail');
    Route::get('leave_type', 'API\APIController@leave_type');
    Route::get('leave_duration', 'API\APIController@leave_duration');
    Route::post('confirm', 'API\APIController@confirm');
    Route::get('conclusion_monthly', 'API\APIController@conclusion_monthly');
    Route::get('jobdescription', 'API\APIController@jobdescription');
    Route::get('employee_level', 'API\APIController@employee_level');
    Route::get('structure_top', 'API\APIController@structure_top');
    Route::post('structure_main', 'API\APIController@structure_main');
    Route::get('get_under', 'API\APIController@get_under');
    Route::post('post_evaluation', 'API\APIController@post_evaluation');
    Route::get('get_evaluation', 'API\APIController@get_evaluation');
    Route::get('get_evaluation/{target_id}', 'API\APIController@get_evaluation_detail');
    Route::get('evaluation_grade_calculate', 'API\APIController@evaluation_grade_calculate');

    // Route::get('get_score_history_new', 'API\APIController@get_score_history_new');
    // Route::get('get_score_history_new/{employee_id}', 'API\APIController@get_score_history_new');

    Route::get('get_score_history', 'API\APIController@get_score_history');
    Route::get('get_score_history/{employee_id}', 'API\APIController@get_score_history');
    Route::get('check_post_evaluation', 'API\APIController@check_post_evaluation');
    Route::post('post_image', 'API\APIController@post_image');
    Route::get('report_registration_leave', 'API\APIController@report_registration_leave');
    Route::post('check_data', 'API\APIController@check_data');
    // START PROJECT
    Route::get('get_employee', 'API\APIController@get_employee');
    Route::post('project_create', 'API\APIController@project_create');
    Route::get('project/{project_id}', 'API\APIController@project');
    Route::post('project/{project_id}', 'API\APIController@project_update');
    Route::post('project_delete/{project_id}', 'API\APIController@project_delete');
    Route::get('project_list', 'API\APIController@project_list');
    Route::post('project_task', 'API\APIController@project_task_create');
    Route::get('project_detail/{project_id}', 'API\APIController@project');
    Route::post('project_task/{project_task_id}', 'API\APIController@project_task_update');
    // END PROJECT

    // START TODO_LIST
    Route::get('todolist_get', 'API\APIController@todolist_get');
    Route::get('todolist_show/{id}', 'API\APIController@todolist_show');
    Route::post('todolist_create/', 'API\APIController@todolist_create');
    Route::post('todolist_update/{id}', 'API\APIController@todolist_update');
    Route::post('todolist_delete/{id}', 'API\APIController@todolist_delete');
    // END TODO_LIST

    Route::get('notification', 'API\NotificationController@index');
    Route::post('sendpushCustom', 'API\NotificationController@sendpushCustom');
    Route::post('sendpushAll', 'API\NotificationController@sendpushAll');
    Route::post('addDevice', 'API\NotificationController@addDevice');
    Route::get('get_log_notification', 'API\NotificationController@get_log_notification');
    Route::post('update_log_notification', 'API\NotificationController@update_log_notification');
    Route::get('count_noti_unread', 'API\NotificationController@count_noti_unread');

    # notification forget leave time
     Route::post('get_notification_forgetleavetimetype', 'API\APIController@get_notification_forgetleavetimetype');
     Route::post('post_notification_forgetleavetime', 'API\APIController@post_notification_forgetleavetime');


   # change picture profile image
   Route::post('change_profilepicture', 'API\APIController@change_profilepicture');
     
});
